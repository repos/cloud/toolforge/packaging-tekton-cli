# Packaging tekton-cli

Packaging boilerplate for tekton-cli (https://github.com/tektoncd/cli)

This CLI is used by the Toolforge Build Service. It's not available in Debian,
so we download a binary from the official GitHub repository, package it in a
.deb file and upload it to the Toolforge Debian repository.

## Building the package

You can just use the build script, pass to it the version of tekton-cli that you want,
and the version of the debian package build, for example:
```
utils/build_deb.sh 0.27.0 2
```

That will build the package `build/tekton-cli-0.27.0-2.deb`.

## Uploading the package

Once built, you can upload the debian package with https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Admin/Packaging#Uploading_a_package
